#include "Headers/Configuration/Global.h"
#include "Headers/ViewerWindow.h"
#include "ui_ViewerWindow.h"
#include "Headers/TestResultFormEntry.h"

void ViewerWindow::postInit() {
    setWindowIcon( QIcon( ":/Resources/Icons/ToasterIcon.ico" ) );
    engine.SetBaseWidget( ui->centralwidget );
    ui->testNameLabel->setText( engine.GetTestName( ) );
    ui->testInfoRTE->setText( engine.GetTestFormatedInformation( ) );
    ui->testNameLabel->AutoCorrectFont( );
    ui->previousButton->hide( );
    ui->answerButton->hide( );
    ui->nextButton->hide( );
    ui->finishTestButton->hide( );

    QObject::connect( ui->startTestButton, SIGNAL( clicked( bool ) ),
                      this, SLOT( StartTest( ) ) );
    QObject::connect( ui->previousButton, SIGNAL( clicked( bool ) ),
                      this, SLOT( PreviousQuestion( ) ) );
    QObject::connect( ui->nextButton, SIGNAL( clicked( bool ) ),
                      this, SLOT( NextQuestion( ) ) );
    QObject::connect( ui->answerButton, SIGNAL( clicked( bool ) ),
                      this, SLOT( NextQuestion( ) ) );
    QObject::connect( ui->finishTestButton, SIGNAL( clicked( bool ) ),
                      this, SLOT( FinishTest( ) ) );
    QObject::connect( &engine, SIGNAL( incompleteAnswer( bool ) ),
                      this, SLOT( IncompleteAnswer( bool ) ) );
    QObject::connect( &engine, SIGNAL( noPreviousQuestion( ) ),
                      this, SLOT( FirstQuestionHandle( ) ) );
    QObject::connect( &engine, SIGNAL( noNextQuestion( ) ),
                      this, SLOT( LastQuestionHandle( ) ) );
}
ViewerWindow::ViewerWindow( QString testFilePath, QWidget* parent ) :
    QMainWindow( parent ),
    ui( new Ui::ViewerWindow ),
    engine( testFilePath )
{
    ui->setupUi( this );
    postInit( );
}
ViewerWindow::~ViewerWindow( ) {
    delete ui;
}
void ViewerWindow::StartTest(  ) {
    ui->testNameLabel->hide( );
    ui->testInfoRTE->hide( );
    ui->startTestButton->hide( );

    if( !engine.IsFreeAnswer( ) ) {
        ui->answerButton->show( );
    } else {
        ui->nextButton->show( );
    }

    engine.NextQuestion( );
}
void ViewerWindow::FirstQuestionHandle( ) {
    ui->previousButton->hide( );
}
void ViewerWindow::PreviousQuestion( ) {
    engine.PreviousQuestion( );
    ui->finishTestButton->hide( );

    if( !engine.IsFreeAnswer( ) ) {
        ui->answerButton->show( );
    } else {
        ui->nextButton->show( );
    }
}
void ViewerWindow::NextQuestion( ) {
    engine.NextQuestion( );
    if( !engine.PreviousForbidden( ) ) {
        ui->previousButton->show( );
    }
}
void ViewerWindow::IncompleteAnswer( bool incomplete ) {
    ui->answerButton->setEnabled( !incomplete );
}
void ViewerWindow::LastQuestionHandle( ) {
    ui->nextButton->hide( );
    ui->answerButton->hide( );
    ui->finishTestButton->show( );
}
void ViewerWindow::FinishTest( ) {
    ui->previousButton->hide( );
    ui->answerButton->hide( );
    ui->nextButton->hide( );
    ui->finishTestButton->hide( );
    engine.ShowResults( );
}

