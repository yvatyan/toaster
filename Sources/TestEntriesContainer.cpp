#include "Headers/TestEntriesContainer.h"

void TestEntriesContainer::addEntryToVisualizer( TestEntryWrapper* wrappedEntryrPtr ) {
    wrappedEntryrPtr->setText( 0, wrappedEntryrPtr->GetConstEntryPtr( )->GetMainBlock( ) );
    int secondaryBlocksCount = wrappedEntryrPtr->GetConstEntryPtr( )->GetSecondaryBlocksQty( );
    for( int i = 0; i < secondaryBlocksCount; ++i ) {
        TestEntryWrapper *secondaryBlock = new TestEntryWrapper( wrappedEntryrPtr );
        secondaryBlock->setText( 0, wrappedEntryrPtr->GetConstEntryPtr( )->GetSecondaryBlocks( ).at( i ) );
    }
}
void TestEntriesContainer::deleteEntry( TestEntryWrapper* wrappedEntryPtr ) {
    for( int i = 0; i < wrappedEntryPtr->childCount( ); ++i ) {
        QTreeWidgetItem* wrappedChild = static_cast<QTreeWidgetItem*>( wrappedEntryPtr->child( i ) );
        static_cast<QTreeWidget*>(visualiserWidget)->removeItemWidget(
                            wrappedChild,
                            0
                    );
        delete wrappedChild;
    }
    static_cast<QTreeWidget*>(visualiserWidget)->removeItemWidget(
                        static_cast<QTreeWidgetItem*>( wrappedEntryPtr ),
                        0
                );
    delete wrappedEntryPtr;
}
void TestEntriesContainer::deleteSecondaryBlock( TestEntryWrapper* secondaryBlockItemPtr ) {
    int secondaryBlockId = secondaryBlockItemPtr->GetId( );
    int wrappedEntryId = static_cast<TestEntryWrapper*>(
                                static_cast<QTreeWidgetItem*>( secondaryBlockItemPtr )->parent( )
                )->GetId( );
    static_cast<QTreeWidget*>(visualiserWidget)->removeItemWidget(
                        static_cast<QTreeWidgetItem*>( secondaryBlockItemPtr ),
                        0
                );
    delete secondaryBlockItemPtr;

    internalContainer[ wrappedEntryId ]->GetEntryPtr( )->DeleteSecondaryBlockAt( secondaryBlockId );
}
TestEntriesContainer::~TestEntriesContainer( ) {
    for(int i = 0; i < internalContainer.size( ); ++i ) {
        deleteEntry( internalContainer.at( i ) );
    }
}
const TestEntry& TestEntriesContainer::operator[]( int index ) const {
    return *( internalContainer.at( index )->GetEntryPtr( ) );
}
void TestEntriesContainer::SetWidget( QWidget* visualiser ) {
    visualiserWidget = visualiser;
}
void TestEntriesContainer::AddEntry( const TestEntry& entry ) {
    TestEntryWrapper* entryWrapper = new TestEntryWrapper( static_cast<QTreeWidget*>( visualiserWidget ), entry );
    entryWrapper->SetId( internalContainer.size( ) );
    internalContainer.append( entryWrapper );

    addEntryToVisualizer( internalContainer.last( ) );
}
void TestEntriesContainer::DeleteEntriesSelectedInVisualizer ( ) {
    QList<QTreeWidgetItem*> selectedItems = static_cast<QTreeWidget*>( visualiserWidget )->selectedItems( );
    for( int i = 0; i < selectedItems.size( ); ++i ) {
        TestEntryWrapper* wrappedEntryPtr = static_cast<TestEntryWrapper*>( selectedItems.at( i ) );
        if( wrappedEntryPtr->GetConstEntryPtr( ) == nullptr ) {
            deleteSecondaryBlock( wrappedEntryPtr );
        } else {
            deleteEntry( wrappedEntryPtr );
            internalContainer.removeAt( wrappedEntryPtr->GetId( ) );
        }
    }
}
