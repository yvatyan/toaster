#-------------------------------------------------
#
# Project created by QtCreator 2018-03-16T15:43:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Toaster
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    Sources/Main.cpp \
    Sources/TestEntry.cpp \
    Sources/TestEntriesContainer.cpp \
    Sources/TestEntryWrapper.cpp \
    Sources/CreatorWindow.cpp \
    Sources/ViewerWindow.cpp \
    Sources/Configuration/Version.cpp \
    Sources/TestQuestionForm.cpp \
    Sources/AutoCorrectableLabel.cpp \
    Sources/TestDatabase.cpp \
    Sources/TestEngine.cpp \
    Sources/TestResultForm.cpp \
    Sources/TestResultFormEntry.cpp

HEADERS += \
    Headers/Configuration/Global.h \
    Headers/TestEntry.h \
    Headers/TestEntriesContainer.h \
    Headers/TestEntryWrapper.h \
    Headers/CreatorWindow.h \
    Headers/ViewerWindow.h \
    Headers/TestQuestionForm.h \
    Headers/AutoCorrectableLabel.h \
    Headers/TestDatabase.h \
    Headers/TestEngine.h \
    Headers/TestQuestionFormBaseClass.h \
    Headers/TestResultForm.h \
    Headers/TestResultFormEntry.h

FORMS += \
    Resources/Forms/CreatorWindow.ui \
    Resources/Forms/ViewerWindow.ui

RESOURCES += \
    Resources.qrc \
    Tests.qrc

RC_FILE = Resources.rc

#ProjectDirectory = G:/Projects/toaster
#Release:DESTDIR = $$ProjectDirectory/Build/Release
#Release:OBJECTS_DIR = $$ProjectDirectory/Build/Release/.obj
#Release:MOC_DIR = $$ProjectDirectory/Build/Release/.moc
#Release:RCC_DIR = $$ProjectDirectory/Build/Release/.rcc
#Release:UI_DIR = $$ProjectDirectory/Build/Release/.ui

#Debug:DESTDIR = $$ProjectDirectory/Build/Debug
#Debug:OBJECTS_DIR = $$ProjectDirectory/Build/Debug/.obj
#Debug:MOC_DIR = $$ProjectDirectory/Build/Debug/.moc
#Debug:RCC_DIR = $$ProjectDirectory/Build/Debug/.rcc
#Debug:UI_DIR = $$ProjectDirectory/Build/Debug/.ui

