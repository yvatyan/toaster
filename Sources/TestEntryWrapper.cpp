#include "Headers/TestEntryWrapper.h"

void TestEntryWrapper::swap( TestEntryWrapper& obj ) {
    std::swap( id, obj.id );
    std::swap( entryPtr, obj.entryPtr );
}
TestEntryWrapper::TestEntryWrapper( QTreeWidgetItem* parent )
    : QTreeWidgetItem( parent )
    , entryPtr( nullptr )
{
}
TestEntryWrapper::TestEntryWrapper( QTreeWidget* parent, const TestEntry& entry )
    : QTreeWidgetItem( parent )
    , entryPtr( new TestEntry( entry ) )
{
}
TestEntryWrapper::TestEntryWrapper( const TestEntryWrapper& copy )
    : QTreeWidgetItem( copy )
    , entryPtr( nullptr )
    , id( copy.id )
{
    if( copy.entryPtr != nullptr ) {
        entryPtr = new TestEntry( *copy.entryPtr );
    }
}
TestEntryWrapper::~TestEntryWrapper( ) {
    if ( entryPtr != nullptr ) {
        delete entryPtr;
    }
}
TestEntryWrapper& TestEntryWrapper::operator=( const TestEntryWrapper& copy ) {
    TestEntryWrapper temp( copy );
    swap( temp );
    return *this;
}
void TestEntryWrapper::SetId( int wrapperId ) {
    id = wrapperId;
}
int TestEntryWrapper::GetId( ) const {
    return id;
}
TestEntry* TestEntryWrapper::GetEntryPtr( ) {
    return entryPtr;
}

const TestEntry* TestEntryWrapper::GetConstEntryPtr( ) const {
    return entryPtr;
}
