#ifndef _YV_TESTENTRIESCONTAINER_H_
#define _YV_TESTENTRIESCONTAINER_H_

#include "Headers/TestEntryWrapper.h"

class TestEntriesContainer {
    public:
        ~TestEntriesContainer( );

        const TestEntry& operator[]( int index ) const;

        void SetWidget( QWidget* visualiser );
        inline int Size( ) const;
        void AddEntry( const TestEntry& entry );    // Adds entry to internal container and visualizer.
        void DeleteEntriesSelectedInVisualizer( );   // Deletes entries from internal container and visualizer.
    private:
        QWidget* visualiserWidget;
        QList<TestEntryWrapper*> internalContainer;

        void addEntryToVisualizer(TestEntryWrapper* wrappedEntryrPtr );  // Visualizer is TreeWidget.
        void deleteEntry( TestEntryWrapper* wrappedEntryPtr );
        void deleteSecondaryBlock( TestEntryWrapper* secondaryBlockItemPtr );
};

int TestEntriesContainer::Size( ) const {
    return internalContainer.size( );
}

#endif // _YV_TESTENTRIESCONTAINER_H_
