#include "Headers/TestEngine.h"
#include <QDebug>
#include <QDateTime>
#include <algorithm>

int TestEngine::randomNumberInRange( int from, int to ) const {
    qsrand( QDateTime::currentDateTime( ).toMSecsSinceEpoch( ) );
    return qrand( ) % (to - from + 1) + from;
}
QList<QString> TestEngine::shuffleList( QList<QString> list ) const {
    std::random_shuffle( list.begin( ), list.end( ) );
    return list;
}
QList<QString> TestEngine::generateVariantsFromEntry( int qty, const TestEntry& entry, QList<QPair<int, int> >& usedIndices ) const {
    QList<QString> variants;
    QPair<int , int> index;
    index.first = entry.GetId( );
    for( int i = 0; i < qty; ++i ) {
        QString variant = entry.GetRandomSecondaryBlock( index.second );
        if( entryIndexExist ( index, usedIndices ) ) {
            --i;
            continue;
        }
        variants.append( variant );
        usedIndices.append( index );        
    }
    return variants;
}
QList<QString> TestEngine::generateVariantsExcludingEntry( int qty, const TestEntry& entry, QList<QPair<int, int> >& usedIndices ) const {
    QList<QString> variants;
    for( int i = 0; i < qty; ++i ) {
        TestEntry newEntry = test.GetRandomEntry( );
        QPair<int, int> index;
        index.first = newEntry.GetId( );
        if( newEntry.GetId( ) == entry.GetId( ) ) {
            --i;
            continue;
        }

        int usedSecondaryBlockCount = 0;
        for(int i = 0; i < usedIndices.size( ); ++i ) {
            if( usedIndices.at( i ).first == newEntry.GetId( ) ) {
                usedSecondaryBlockCount ++;
            }
        }
        if( usedSecondaryBlockCount == newEntry.GetSecondaryBlocksQty( ) ) {
            --i;
            continue;
        }
        while( 8 ) {
            QString variant = newEntry.GetRandomSecondaryBlock( index.second );
            if( !entryIndexExist( index, usedIndices ) ) {
                variants.append( variant );
                usedIndices.append( index );
                break;
            }
        }
    }
    return variants;
}
bool TestEngine::entryIndexExist( QPair<int, int> index, QList<QPair<int, int> > usedIndices ) const {
    for(int i = 0; i < usedIndices.size( ); ++i ) {
        if( index.first == usedIndices.at( i ).first ) {
            if( index.second == usedIndices.at( i ).second ) {
                return true;
            }
        }
    }
    return false;
}
void TestEngine::generateQuestion( ) {
    QFont questionFont( "Times", 14, QFont::Bold );
    QFont variantFont( "Times", 10 );
    QRect formSize( 65, 50, 350, 350 );
    TestQuestionFormBaseClass* testForm;

    TestEntry entry;
    bool run = true;
    while( run ) {
        entry = TestEntry( test.GetRandomEntry( ) );
        run = false;
        for(int i = 0; i < usedEntries.size( ); ++i ) {
            if( entry.GetId( ) == usedEntries.at( i ) ) {
                run = true;
                break;
            }
        }
    }
    if( entry.GetQuestionFormat( ) == TestEntry::SelectOne ) {
        testForm = new TestQuestionForm<QRadioButton>(
                    usedEntries.size( ) + 1,
                    275,        // TODO: make it customizable
                    formSize,   // TODO: make it customizable
                    baseWidget );
    } else if( entry.GetQuestionFormat( ) == TestEntry::SelectMultiple ) {
        testForm = new TestQuestionForm<QCheckBox>(
                    usedEntries.size( ) + 1,
                    275,        // TODO: make it customizable
                    formSize,   // TODO: make it customizable
                    baseWidget );
    } else if( entry.GetQuestionFormat( ) == TestEntry::TextInput ) {
        testForm = new TestQuestionForm<QLineEdit>(
                    usedEntries.size( ) + 1,
                    275,        // TODO: make it customizable
                    formSize,   // TODO: make it customizable
                    baseWidget );
    }
    pages.append( QPair<TestQuestionFormBaseClass*, int>( testForm, entry.GetId( ) ) );
    usedEntries.append( entry.GetId( ) );

    testForm->SetQuestion( entry.GetMainBlock( ), questionFont );

    QList<QString> variants;
    if( entry.GetQuestionFormat( ) == TestEntry::SelectOne ||
            entry.GetQuestionFormat( ) == TestEntry::SelectMultiple ) {
        int rightVariantQty;
        if( entry.GetQuestionFormat( ) == TestEntry::SelectOne ) {
            rightVariantQty = 1;
        } else if( entry.GetQuestionFormat( ) == TestEntry::SelectMultiple ) {
            rightVariantQty = randomNumberInRange( 1, std::min( variantQty, entry.GetSecondaryBlocksQty( ) ) );
        }
        QList< QPair<int, int> > usedIndices;
        QList<QString> rightVariants = generateVariantsFromEntry(
                        rightVariantQty,
                        entry,
                        usedIndices
                    );
        QList<QString> additionalVariants = generateVariantsExcludingEntry(
                        variantQty - rightVariants.size( ),
                        entry,
                        usedIndices
                    );
        variants = shuffleList( rightVariants + additionalVariants );
    } else if( entry.GetQuestionFormat( ) == TestEntry::TextInput ) {
        variants.append( "" );
    }

    for( int i = 0; i < variants.size( ); ++i ) {
        testForm->AddVariant( variants.at( i ), variantFont );
    }
    testForm->setGeometry( 65, 50, 350, 340 );
}
void TestEngine::generateTestResult( ) {
    resultForm = new TestResultForm( baseWidget );
    resultForm->setGeometry( 65, 50, 350, 540 );

    for(int i = 0; i < pages.size( ); ++i ) {
        TestEntry entry = test.GetEntryByIndex( pages.at( i ).second );
        QList<QPair<QString, bool> > variants = pages.at( i ).first->GetVariants( );
        QString question = pages.at( i ).first->GetQuestion( );
        QString answer;
        QString rightAnswer;
        bool rightAnswered = false;
        for( int i = 0; i < variants.size( ); ++i ) {
            if( variants.at( i ).second ) {
                answer += variants.at( i ).first + ", ";
            }
            if( variants.at( i ).first == "" ) {
                for( QString rightVariant : entry.GetSecondaryBlocks( ) ) {
                    rightAnswer += rightVariant + ", ";
                }
            } else {
                if( entry.IsVariantRight( variants.at( i ).first ) ) {
                    rightAnswer += variants.at( i ).first + ", ";
                    if( variants.at( i ).second ) {
                        rightAnswered = true;
                    }
                }
            }
        }
        if( !answer.isEmpty( ) ) {
            answer.remove( answer.size( ) - 2, 2 );
        }
        if( answer.isEmpty( ) ) {
            answer = "[Empty answer]";
        }
        if( !rightAnswer.isEmpty( ) ) {
            rightAnswer.remove( rightAnswer.size( ) - 2, 2 );
        }
        resultForm->AddEntry( rightAnswered,
                              question,
                              answer,
                              rightAnswer );
    }
    resultForm->show( );
}
TestEngine::TestEngine( QString pathToTestFile ) {
    currentQuestion = -1;
    variantQty = 4;
    test.SetFilePath( pathToTestFile );
    test.LoadFromFile( );
}
TestEngine::~TestEngine( ) {
    for( int i = pages.size( ) - 1; i >= 0; --i ) {
        qDebug( ) << "Index:" << i;
        delete pages.at( i ).first;
    }
    qDebug() << "Test engine";
}
void TestEngine::PreviousQuestion( ) {
    currentQuestion --;
    if( currentQuestion == -1 ) {
        currentQuestion ++;
        return;
    }
    if( currentQuestion == 0 ) {
        emit noPreviousQuestion( );
    }
    if(currentQuestion != test.GetQuestionQty() - 1 ) {
        pages.at( currentQuestion + 1 ).first->hide( );
    }
    pages.at( currentQuestion ).first->show( );
}
void TestEngine::NextQuestion( ) {
    currentQuestion ++;
    if( currentQuestion == test.GetQuestionQty( ) ) {
        currentQuestion --;
        return;
    }
    if( currentQuestion == test.GetQuestionQty( ) - 1 ) {
        emit noNextQuestion( );
    }
    if( currentQuestion == pages.size( ) ) {
        generateQuestion( );
    }
    if( currentQuestion != 0 ) {
        pages.at( currentQuestion - 1 ).first->hide( );
    }
    pages.at( currentQuestion ).first->show( );
}
void TestEngine::ShowResults( ) {
    pages.at( currentQuestion ).first->hide( );
    generateTestResult( );
}
void TestEngine::SetBaseWidget( QWidget* base ) {
    baseWidget = base;
}
QString TestEngine::GetTestName( ) const {
    return test.GetTestName( );
}
QString TestEngine::GetTestFormatedInformation( ) const {
    QList<QPair<QString, QString> > fields = test.GetTestInfoFields( );
    QString formated;
    for( int i = 0; i < fields.size( ); ++i ) {
        formated += fields.at( i ).first + ": <b>" + fields.at( i ).second + "</b><br>";
    }
    return formated;
}
bool TestEngine::IsFreeAnswer( ) const {
    return !test.IsAnswerForced( );
}
bool TestEngine::PreviousForbidden( ) const {
    return test.GetAnswerPolicy() == TestDatabase::ForbidPrevious;
}
void TestEngine::onIncompleteAnswer( bool noAnswer ) {
    if( !IsFreeAnswer( ) ) {
        emit incompleteAnswer( noAnswer );
    }
}
