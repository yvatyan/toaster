#ifndef _YV_TESTENGINE_H_
#define _YV_TESTENGINE_H_

#include <QString>
#include <QTextEdit>
#include "Headers/TestDatabase.h"
#include "Headers/TestQuestionForm.h"
#include "Headers/TestResultForm.h"

class TestEngine : public QObject {
    Q_OBJECT
    public:
        TestEngine( QString pathToTestFile );
        ~TestEngine( );

        void PreviousQuestion( );
        void NextQuestion( );
        void ShowResults( );

        void SetBaseWidget( QWidget* base );

        QString GetTestName( ) const;
        QString GetTestFormatedInformation( ) const;
        bool IsFreeAnswer( ) const;
        bool PreviousForbidden( ) const;
    private:
        TestDatabase test;
        QWidget* baseWidget;
        QList<QPair<TestQuestionFormBaseClass*, int> > pages;
        TestResultForm* resultForm;
        QList<int> usedEntries;
        int currentQuestion;
        int variantQty;

        void generateQuestion( );
        void generateTestResult( );

        int randomNumberInRange( int from, int to ) const;
        QList<QString> shuffleList( QList<QString> list ) const;
        QList<QString> generateVariantsFromEntry( int qty, const TestEntry& entry, QList<QPair<int, int> >& usedIndices ) const;
        QList<QString> generateVariantsExcludingEntry( int qty, const TestEntry& entry, QList<QPair<int, int> >& usedIndices ) const;
        bool entryIndexExist( QPair<int, int> index, QList<QPair<int, int> > usedIndices ) const;
    public slots:
        void onIncompleteAnswer( bool noAnswer );
    signals:
        void incompleteAnswer( bool noAnswer );
        void noPreviousQuestion( );
        void noNextQuestion( );
};

#endif // _YV_TESTENGINE_H_
