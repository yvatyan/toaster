#ifndef _YV_TESTENTRY_H_
#define _YV_TESTENTRY_H_

#include <QString>
#include <QList>

class TestEntry {
    public:
        enum Source {
            None = 0,
            MainBlock = 1,
            FirstEntryOfSecBlock = 2,
            SecondaryBlock = 4
        };
        enum Format {
            SelectOne,
            SelectMultiple,
            TextInput
        };

        void SetMainBlock( QString string );
        void SetQuestionSourceMask( unsigned char srcMask );
        void SetQuestionFormat( Format fmt );
        void SetId( int i );

        QString GetMainBlock( ) const;
        unsigned char GetQuestionSourceMask( ) const;
        QString GetQuestionSourceMaskStr( ) const;
        Format GetQuestionFormat( ) const;
        char GetQuestionFormatStr( ) const;
        int GetSecondaryBlocksQty( ) const;
        const QList<QString>& GetSecondaryBlocks( ) const;
        QString GetSecondaryBlockAt( int index ) const;
        QString GetRandomSecondaryBlock( int& index ) const;
        int GetId( ) const;

        bool IsVariantRight( QString variant ) const;

        void AddSecondaryBlock( QString string );
        void DeleteSecondaryBlockAt( int index );
    private:
        QString mainBlock;
        QList<QString> secondaryBlocks;
        unsigned char questionSourceMask;
        Format questionFormat;
        int id;
};

#endif // _YV_TESTENTRY_H_
