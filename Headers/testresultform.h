#ifndef _YV_TESTRESULTFORM_H_
#define _YV_TESTRESULTFORM_H_

#include <QWidget>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QString>

#include "Headers/TestResultFormEntry.h"

class TestResultForm : public QWidget {
    public:
        TestResultForm( QWidget* parent = nullptr );
        ~TestResultForm( );

        void SetHeader( QString headerText );
        void setGeometry( int x, int y, int width, int height );

        void AddEntry( bool isRight, QString question, QString selectedVariant, QString rightVariant = "" );

        void show( );
        void hide( );

    private:
        bool showRightAnswer;
        QWidget* baseWidget;
        QScrollArea* scrollArea;
        QVBoxLayout* vLayout;

        QList<TestResultFormEntry*> entries;
};

#endif // _YV_TESTRESULTFORM_H_
