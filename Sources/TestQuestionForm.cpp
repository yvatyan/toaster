#include "Headers/TestQuestionForm.h"

template <>
void TestQuestionForm<QCheckBox>::AddVariant( QString variant, const QFont& font ) {
    questions.append( QPair<QCheckBox*, QTextEdit*>( ) );
    QTextEdit* newVariant = new QTextEdit( variant );
    newVariant->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    newVariant->setReadOnly( true );
    newVariant->setCursor( Qt::ArrowCursor );
    newVariant->setTextInteractionFlags( Qt::NoTextInteraction );
    setTextEditFont( newVariant, font );
    updateTextEditHeight( newVariant, textEditWidth );
    gLayout->addWidget( newVariant, questions.size( ), 1, Qt::AlignLeft );
    questions.last( ).second = newVariant;

    QCheckBox* newSelector = new QCheckBox( "" );
    newSelector->setFixedSize( 25, 25 );
    gLayout->addWidget( newSelector, questions.size( ), 0, Qt::AlignLeft );
    QObject::connect( newSelector, SIGNAL( toggled( bool ) ), this, SLOT( onAnswerChange( ) ) );
    questions.last( ).first = newSelector;
}
template <>
void TestQuestionForm<QRadioButton>::AddVariant( QString variant, const QFont& font ) {
    questions.append( QPair<QRadioButton*, QTextEdit*>( ) );
    QTextEdit* newVariant = new QTextEdit( variant );
    newVariant->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    newVariant->setReadOnly( true );
    newVariant->setCursor( Qt::ArrowCursor );
    newVariant->setTextInteractionFlags( Qt::NoTextInteraction );
    setTextEditFont( newVariant, font );
    updateTextEditHeight( newVariant, textEditWidth );
    gLayout->addWidget( newVariant, questions.size( ), 1, Qt::AlignLeft );
    questions.last( ).second = newVariant;

    QRadioButton* newSelector = new QRadioButton( "" );
    newSelector->setFixedSize( 25, 25 );
    gLayout->addWidget( newSelector, questions.size( ), 0, Qt::AlignLeft );
    QObject::connect( newSelector, SIGNAL( toggled( bool ) ), this, SLOT( onAnswerChange( ) ) );
    questions.last( ).first = newSelector;
}
template <>
void TestQuestionForm<QLineEdit>::AddVariant( QString variant, const QFont& font ) {
    questions.append( QPair<QLineEdit*, QTextEdit*>( ) );
    QTextEdit* newVariant = nullptr;
    if( !variant.isEmpty( ) ) {
        newVariant = new QTextEdit( variant );
        newVariant->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
        newVariant->setReadOnly( true );
        newVariant->setCursor( Qt::ArrowCursor );
        newVariant->setTextInteractionFlags( Qt::NoTextInteraction );
        setTextEditFont( newVariant, font );
        updateTextEditHeight( newVariant, textEditWidth );
        gLayout->addWidget( newVariant, questions.size( ), 1, Qt::AlignLeft );
    }
    questions.last( ).second = newVariant;

    QLineEdit* newSelector = new QLineEdit( "" );
    if( newVariant == nullptr ) {
        newSelector->setFixedSize( width( ) / 2 + 0.5 * ( textEditWidth + 25 ), 25 );
        gLayout->addWidget( newSelector, questions.size( ), 0, 1, 2, Qt::AlignLeft );
    } else {
        newSelector->setFixedSize( 25, 25 );
        gLayout->addWidget( newSelector, questions.size( ), 0, Qt::AlignLeft );
    }
    QObject::connect( newSelector, SIGNAL( editingFinished( ) ), this, SLOT( onAnswerChange( ) ) );
    questions.last( ).first = newSelector;
}
////////////////////////////////////////////////////////////////////////////////////////
template <>
QList<QPair<QString, bool> > TestQuestionForm<QCheckBox>::GetVariants( ) const {
    QList<QPair<QString, bool> > variants;
    for(int i = 0; i < questions.size( ); ++i ) {
        variants.append( QPair<QString, bool>( questions.at( i ).second->document( )->toPlainText( ),
                                               questions.at( i ).first->isChecked( ) ) );
    }
    return variants;
}
template <>
QList<QPair<QString, bool> > TestQuestionForm<QRadioButton>::GetVariants( ) const {
    QList<QPair<QString, bool> > variants;
    for(int i = 0; i < questions.size( ); ++i ) {
        variants.append( QPair<QString, bool>( questions.at( i ).second->document( )->toPlainText( ),
                                               questions.at( i ).first->isChecked( ) ) );
    }
    return variants;
}
template <>
QList<QPair<QString, bool> > TestQuestionForm<QLineEdit>::GetVariants( ) const {
    QList<QPair<QString, bool> > variants;
    for(int i = 0; i < questions.size( ); ++i ) {
        QString variant = questions.at( i ).first->text( ).trimmed( );
        variants.append( QPair<QString, bool>( variant, true ) );
    }
    qDebug() << "QLine edit" << variants;
    return variants;
}
////////////////////////////////////////////////////////////////////////////////////////
template <>
void TestQuestionForm<QCheckBox>::onAnswerChangeImpl( ) const {
    bool emptyAnswer = true;
    for( int i = 0; i < questions.size( ); ++i ) {
        if( questions.at( i ).first->isChecked( ) ) {
            emptyAnswer = false;
            break;
        }
    }
    emit answerIsEmpty( emptyAnswer );
}
template <>
void TestQuestionForm<QRadioButton>::onAnswerChangeImpl( ) const {
    bool emptyAnswer = true;
    for( int i = 0; i < questions.size( ); ++i ) {
        if( questions.at( i ).first->isChecked( ) ) {
            emptyAnswer = false;
            break;
        }
    }
    emit answerIsEmpty( emptyAnswer );
}
template <>
void TestQuestionForm<QLineEdit>::onAnswerChangeImpl( ) const {
    bool emptyAnswer = true;
    for( int i = 0; i < questions.size( ); ++i ) {
        if( !questions.at( i ).first->text( ).isEmpty() ) {
            emptyAnswer = false;
        }
    }
    emit answerIsEmpty( emptyAnswer );
}
