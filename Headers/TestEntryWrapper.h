#ifndef _YV_TESTENTRYWRAPPER_H_
#define _YV_TESTENTRYWRAPPER_H_

#include <QTreeWidgetItem>
#include "Headers/TestEntry.h"

class TestEntryWrapper : public QTreeWidgetItem {
    public:
        TestEntryWrapper( QTreeWidgetItem* parent );
        TestEntryWrapper( QTreeWidget* parent, const TestEntry& entry );
        TestEntryWrapper( const TestEntryWrapper& copy );
        ~TestEntryWrapper( );

        TestEntryWrapper& operator=( const TestEntryWrapper& copy );

        void SetId( int wrapperId );
        int GetId( ) const;
        TestEntry* GetEntryPtr( );
        const TestEntry* GetConstEntryPtr( ) const;
    private:
        TestEntry* entryPtr;
        int id;

        void swap( TestEntryWrapper& obj );
};

#endif // _YV_TESTENTRYWRAPPER_H_
