#ifndef _YV_TESTQUESTIONFORM_H_
#define _YV_TESTQUESTIONFORM_H_

#include <QWidget>
#include <QScrollArea>
#include <QGridLayout>
#include <QTextEdit>
#include <QCheckBox>
#include <QRadioButton>
#include <QLineEdit>
#include <QPair>
#include <QDebug>
#include "Headers/TestQuestionFormBaseClass.h"

template <typename SelectorType>
class TestQuestionForm : public TestQuestionFormBaseClass {
    public:
        TestQuestionForm( int questionNumber, int boxWidth, QRect geometry, QWidget* parent = nullptr );
        ~TestQuestionForm( );

        void SetQuestion(QString question, const QFont &font );
        QString GetQuestion( ) const;
        void setGeometry( int x, int y, int width, int height, int boxWidth = -1 );
        void setGeometry( QRect boundary, int boxWidth = -1 );
        void AddVariant( QString variant, const QFont &font ) {}

        QList<QPair<QString, bool> > GetVariants( ) const;

        void show( );
        void hide( );
    protected:
        void onAnswerChangeImpl( ) const;
    private:
        QTextEdit* questionTE;
        QWidget* baseWidget;
        QScrollArea* scrollArea;        
        QList<QPair<SelectorType*, QTextEdit*> > questions;
        QGridLayout* gLayout;
        int textEditWidth;
        int index;

        void updateTextEditHeight(QTextEdit* te , int width );
        void setTextEditFont( QTextEdit* te, const QFont& font );
};

template<>
void TestQuestionForm<QCheckBox>::AddVariant( QString variant, const QFont &font );
template<>
void TestQuestionForm<QRadioButton>::AddVariant( QString variant, const QFont &font );
template<>
void TestQuestionForm<QLineEdit>::AddVariant( QString variant, const QFont &font );

template<>
QList<QPair<QString, bool> > TestQuestionForm<QCheckBox>::GetVariants( ) const;
template<>
QList<QPair<QString, bool> > TestQuestionForm<QRadioButton>::GetVariants( ) const;
template<>
QList<QPair<QString, bool> > TestQuestionForm<QLineEdit>::GetVariants( ) const;

template<>
void TestQuestionForm<QCheckBox>::onAnswerChangeImpl( ) const;
template<>
void TestQuestionForm<QRadioButton>::onAnswerChangeImpl( ) const;
template<>
void TestQuestionForm<QLineEdit>::onAnswerChangeImpl( ) const;

template <typename SelectorType>
TestQuestionForm<SelectorType>::TestQuestionForm(int questionNumber, int boxWidth , QRect geometry, QWidget* parent )
    : TestQuestionFormBaseClass( parent )
{
    questionTE = new QTextEdit ( this );
    questionTE->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    questionTE->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    scrollArea = new QScrollArea( this );
    scrollArea->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    baseWidget = new QWidget;
    gLayout = new QGridLayout;
    index = questionNumber;
    setGeometry( geometry, boxWidth );
}
template <typename SelectorType>
TestQuestionForm<SelectorType>::~TestQuestionForm( ) {
    qDebug( ) << "Form Class: begin";
    if( questions.size( ) ) {
        for( int i = 0; i < questions.size( ); ++i ){
            if( questions.at( i ).first != nullptr ) {
                delete questions.at( i ).first;
            }
            if( questions.at( i ).second != nullptr ) {
                delete questions.at( i ).second;
            }
        }
    }
    delete gLayout;
    delete baseWidget;
    delete scrollArea;
    delete questionTE;
    qDebug( ) << "Form Class: end";
}
template <typename SelectorType>
void TestQuestionForm<SelectorType>::SetQuestion( QString question, const QFont& font ) {
    questionTE->setText( QString::number( index ) + ". " + question );
    setTextEditFont( questionTE, font );
    questionTE->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    questionTE->setReadOnly( true );
    questionTE->setCursor( Qt::ArrowCursor );
    questionTE->setTextInteractionFlags( Qt::NoTextInteraction );
    setGeometry( geometry( ) );
}
template <typename SelectorType>
QString TestQuestionForm<SelectorType>::GetQuestion( ) const {
    return questionTE->document( )->toPlainText( );
}
template <typename SelectorType>
void TestQuestionForm<SelectorType>::setGeometry( QRect boundary, int boxWidth ) {
    setGeometry( boundary.x( ), boundary.y( ), boundary.width( ), boundary.height( ), boxWidth );
}
template <typename SelectorType>
void TestQuestionForm<SelectorType>::setGeometry( int x, int y, int width, int height, int boxWidth  ) {
    if( boxWidth != -1 ) {
        textEditWidth = boxWidth;
    }
    questionTE->setGeometry( 0, 0, 666, 666 ); // Update only position
    updateTextEditHeight( questionTE, width );
    QWidget::setGeometry( x, y, width, questionTE->height( ) + height );
    scrollArea->setGeometry( 0, questionTE->height( ), width, height - questionTE->height( ) );
    for( int i = 0; i < questions.size( ); ++i ) {
        if( questions.at( i ).second != nullptr ) {
            updateTextEditHeight( questions.at( i ).second, textEditWidth );
        } else {
            questions.at( i ).first->setFixedSize( this->width( ) / 2 + 0.5 * ( textEditWidth + 25 ), 25 );
        }
    }
}
template <typename SelectorType>
void TestQuestionForm<SelectorType>::show( ) {
    baseWidget->setLayout( gLayout );
    scrollArea->setWidget( baseWidget );
    for( int i = 0; i < questions.size( ); ++i ) {
        if( questions.at( i ).second != nullptr ) {
            questions.at( i ).second->show( );
        }
    }
    questionTE->show( );
    QWidget::show( );
}
template <typename SelectorType>
void TestQuestionForm<SelectorType>::hide( ) {
    for( int i = 0; i < questions.size( ); ++i ) {
        if( questions.at( i ).second != nullptr ) {
            questions.at( i ).second->hide( );
        }
    }
    questionTE->hide( );
    QWidget::hide( );
}
template <typename SelectorType>
void TestQuestionForm<SelectorType>::updateTextEditHeight( QTextEdit* te, int width ) {
    te->setFixedSize( width, 666 ); // Update width
    QObject* savedParent = te->parent( );
    te->setParent( static_cast<QWidget*>( parent( ) ) ); // Set widget to render to and obtain real heigth
    te->show( );
    QSize realSize( width, (int)te->document( )->size( ).height( ) + 3 ); // Obtain new height
    te->setFixedSize( realSize );
    te->setParent( static_cast<QWidget*>( savedParent ) );
}
template <typename SelectorType>
void TestQuestionForm<SelectorType>::setTextEditFont( QTextEdit* te, const QFont& font ) {
    QTextCursor cursor = te->textCursor( );
    te->selectAll( );
    te->setFont( font );
    te->setTextCursor( cursor );
}

#endif // _YV_TESTQUESTIONFORM_H_
