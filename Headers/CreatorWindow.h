#ifndef _YV_CREATORWINDOW_H_
#define _YV_CREATORWINDOW_H_

#include <QMainWindow>
#include "Headers/TestDatabase.h"

namespace Ui {
    class CreatorWindow;
}

class CreatorWindow : public QMainWindow {
    Q_OBJECT
    public:
        explicit CreatorWindow(QWidget* parent = 0);
        ~CreatorWindow();

    public slots:
        void ConstructAndAddEntry( );
        void ConstructAndSaveTest( );
        void SelectFileFromDialog( );

    private:
        Ui::CreatorWindow* ui;
        TestDatabase test;
        void postInit( );
};

#endif // _YV_CREATORWINDOW_H_
