#include "Headers/TestDatabase.h"
#include <QDateTime>
#include <string>

void TestDatabase::parseHeaderLine( QString line ) {
    if( line.at( 0 ) != ':' ) {
        // "Invalid header"
        return;
    }
    line = line.remove(0, 1);
    QList<QString> headerArgs = line.split( "\\", QString::SkipEmptyParts );
    name = headerArgs.at( 0 );
    bool ok = false;
    questionQty = headerArgs.at( 1 ).toInt( &ok );
    if( !ok ) {
        // "Invalid question quantity specified: can\'t convert to integer.
        return;
    }
    if( headerArgs.at( 2 ) != "C" && headerArgs.at( 2 ) != "F" ) {
        // Invalid answer policy specified.
    }
    answerPolicy = ( headerArgs.at( 2 ) == "C" ? Custom : ForbidPrevious );
    if( headerArgs.at( 3 ) != "force" && headerArgs.at( 3 ) != "free" ) {
        // Invalid force answer field.
        return;
    }
    forcedAnswer = ( headerArgs.at( 3 ) == "force" );
}
void TestDatabase::parseEntryLine( QString line ) {
    TestEntry entry;
    int mask = 0;
    for( int i = 2; i >= 0; --i ) {
        if( line.at( i ) != 'v' && line.at( i ) != 'x' ) {
            // "Invalid question source mask:" << line.at( i ) << ".";
            return;
        }
        if( line.at( i ) == 'v' ) {
            mask |= 1;
        }
        mask << 1;
    }
    entry.SetQuestionSourceMask( mask );
    if( line.at( 3 ) == "O" ) {
        entry.SetQuestionFormat( TestEntry::SelectOne );
    } else if( line.at( 3 ) == "M" ) {
        entry.SetQuestionFormat( TestEntry::SelectMultiple );
    } else if( line.at( 3 ) == "T" ) {
        entry.SetQuestionFormat( TestEntry::TextInput );
    } else {
        // "Invalid question format for entry:" << line.at( 4 ) << ".";
        return;
    }
    line = line.remove(0, 5);
    QList<QString> mainAndSecondaryBlocks = line.split( "-", QString::SkipEmptyParts );
    QList<QString> mainBlocks = mainAndSecondaryBlocks.at( 0 ).split( "·", QString::SkipEmptyParts );
    if( mainBlocks.size( ) == 0 ) {
        // "Main block can\'t be empty.";
        return;
    }
    if( mainBlocks.size( ) > 1 ) {
        // "Multiple main blocks are not supported.";
        return;
    }
    entry.SetMainBlock( mainBlocks.at( 0 ).trimmed( ) );
    QList<QString> secondaryBlocks = mainAndSecondaryBlocks.at( 1 ).split( "·", QString::SkipEmptyParts );
    if( secondaryBlocks.size( ) == 0 ) {
        // "Secondary blocks can\'t be absent.";
        return;
    }
    for( int i = 0; i < secondaryBlocks.size( ); ++i ) {
        entry.AddSecondaryBlock( secondaryBlocks.at( i ).trimmed( ) );
    }
    AddEntry( entry );
}
TestDatabase::TestDatabase( ) {
    answerPolicy = None;
    forcedAnswer = true;
}
void TestDatabase::AddEntry( TestEntry entry ) {
    entry.SetId( entries.Size( ) );
    entries.AddEntry( entry );
}
void TestDatabase::SetEntriesVisualizerWidget( QWidget* dataVisualizerWidget ) {
    entries.SetWidget( dataVisualizerWidget );
}
void TestDatabase::SetTestName( QString testName ) {
    name = testName;
}
void TestDatabase::SetQuestionsQty( int quantity ) {
    questionQty = quantity;
}
void TestDatabase::SetAnswerPolicy( Policy policy ) {
    answerPolicy = policy;
}
void TestDatabase::EnableForceAnswer( bool force ) {
    forcedAnswer = force;
}
void TestDatabase::SetFilePath( QString path ) {
    filePath = path;
}
void TestDatabase::LoadFromFile( ) {
    std::fstream testFile( filePath.toUtf8( ).constData( ), std::ios_base::in );
    std::string line;
    int index = 0;
    while( std::getline( testFile, line ) ) {
        if( index == 0 ) {
            parseHeaderLine( QString( line.c_str( ) ) );
        } else {
            parseEntryLine( QString( line.c_str( ) ) );
        }
        ++ index;
    }
    testFile.close( );
}
TestEntry TestDatabase::GetEntryByIndex( int index ) const {
    return entries[ index ];
}
TestEntry TestDatabase::GetRandomEntry( ) const {
    qsrand( QDateTime::currentDateTime( ).toMSecsSinceEpoch( ) );
    return GetEntryByIndex( qrand( ) % entries.Size( ) );
}
int TestDatabase::GetEntriesQty( )const {
    return entries.Size( );
}
QString TestDatabase::GetTestName( ) const {
    return name;
}
int TestDatabase::GetQuestionQty( ) const {
    return questionQty;
}
TestDatabase::Policy TestDatabase::GetAnswerPolicy( ) const {
    return answerPolicy;
}
QString TestDatabase::GetAnswerPolicyDescription( ) const {
    if( answerPolicy == Custom ) {
        return QString( "Custom answering direction." );
    } else if( answerPolicy == ForbidPrevious ) {
        return QString( "Answering previous questions forbided." );
    }
    return "Test is not set.";
}
QList<QPair<QString, QString> > TestDatabase::GetTestInfoFields( ) const {
    QList<QPair<QString, QString> > result;
    result << QPair<QString, QString>( "Entry quantity in test", QString::number( entries.Size( ) ) );
    result << QPair<QString, QString>( "Question quantity in test", QString::number( questionQty ) );
    result << QPair<QString, QString>( "Answering policy", GetAnswerPolicyDescription( ) );
    result << QPair<QString, QString>( "Answering is forced",  forcedAnswer ? "true" : "false" );
    return result;
}
bool TestDatabase::IsAnswerForced( ) const {
    return forcedAnswer;
}
void TestDatabase::SaveToFile( ) const {
    std::fstream testFile( filePath.toUtf8( ).constData( ), std::ios_base::out );
    testFile << ":" << name.toUtf8( ).constData( ) << "\\"
                    << questionQty << "\\"
                    << ( answerPolicy == Custom ? "C\\" : "F\\" )
                    << ( forcedAnswer ? "force" : "free" )
                    << std::endl;
    for( int i = 0; i < entries.Size( ); ++i ) {
        testFile << entries[ i ].GetQuestionSourceMaskStr( ).toUtf8( ).constData( )
                 << entries[ i ].GetQuestionFormatStr( ) << " "
                 << entries[ i ].GetMainBlock( ).toUtf8( ).constData( ) << " - ";
        for( int j = 0; j < entries[ i ].GetSecondaryBlocksQty( ); ++j ) {
            testFile << entries[ i ].GetSecondaryBlocks( ).at( j ).toUtf8( ).constData( )
                     << ( j < entries[ i ].GetSecondaryBlocksQty( ) - 1 ? "·" : "");
        }
        testFile << std::endl;
    }
    testFile.close( );
}
