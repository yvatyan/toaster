#include "Headers/TestResultFormEntry.h"

TestResultFormEntry::TestResultFormEntry( bool isRight,
                                          QString question,
                                          QString selectedVariant,
                                          QWidget* parent,
                                          QString rightAnswer ) :
    QWidget( parent )
{
    base = new QTextEdit( this );
    base->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    base->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    base->setReadOnly( true );
    base->setCursor( Qt::WaitCursor );
    this->setCursor( Qt::WaitCursor );
    QWidget::setCursor( Qt::WaitCursor );
    base->setTextInteractionFlags( Qt::NoTextInteraction );
    base->setFrameShadow( QFrame::Plain );
    base->setFrameShape( QFrame::NoFrame );
    base->setLineWidth( 0 );
    setupBaseText( isRight, question, selectedVariant, rightAnswer );
}
TestResultFormEntry::~TestResultFormEntry( ) {
    delete base;
}
void TestResultFormEntry::updateTextEditHeight( QTextEdit* te, int width ) {
    te->setFixedSize( width, 666 ); // Update width
    QObject* savedParent = te->parent( );
    te->setParent( static_cast<QWidget*>( parent( ) ) ); // Set widget to render to and obtain real heigth
    te->show( );
    QSize realSize( width, (int)te->document( )->size( ).height( ) + 3 ); // Obtain new height
    te->setFixedSize( realSize );
    te->setParent( static_cast<QWidget*>( savedParent ) );
    te->hide( );
}
void TestResultFormEntry::setGeometry( int x, int y, int width ) {
    base->setGeometry( 0, 0, width, 666 );
    updateTextEditHeight( base, width );
    QWidget::setGeometry( x, y, width, base->height( ) );
    updateGeometry( );
}
QString TestResultFormEntry::enumColorToString( Color color ) {
    switch( color ) {
        case Red: return "red";
        case Green: return "green";
        case Blue: return "blue";
        case Black: return "black";
        default: return "undefined color";
    }
}
QString TestResultFormEntry::enumWeightToString( Weight weight ) {
    switch( weight ) {
        case Normal: return "normal";
        case Bold: return "bold";
        default: return "undefined weight";
    }
}
void TestResultFormEntry::setupBaseText( bool isRight,
                                         QString question,
                                         QString selectedVariant,
                                         QString rightAnswer ) {
    static const QString wrongIconPath = ":/Resources/Decorations/wrong.png";
    static const QString correctIconPath = ":/Resources/Decorations/correct.png";

    QString htmlText;
    addImageToHtmlText( htmlText, isRight ? correctIconPath : wrongIconPath );
    htmlText += obtainCustomizedHtmlString( question, 14, Black, Bold );
    htmlText += "<p></p>";
    htmlText += obtainCustomizedHtmlString( selectedVariant, 14, isRight ? Green : Red, Normal );
    if( !rightAnswer.isEmpty( ) ) {
        htmlText += "<p></p>"; // <br> tag renders empty line between all lines except the last two.
        htmlText += obtainCustomizedHtmlString( rightAnswer, 14, Blue, Normal );
    }
    base->setHtml( htmlText );
}
QString TestResultFormEntry::obtainCustomizedHtmlString( QString text,
                                                         int fontSize,
                                                         Color fontColor,
                                                         Weight fontWeight ) {
    static const QString htmlStringSample = "<pr style=\"font-size:%font-size%px;"
                                            "color:%font-color%;"
                                            "font-weight:%font-weight%\">"
                                            "%text%</pr>";

    QString result = htmlStringSample;
    result = result.replace( "%font-size%", QString::number( fontSize ) );
    result = result.replace( "%font-color%", enumColorToString( fontColor ) );
    result = result.replace( "%font-weight%", enumWeightToString( fontWeight ) );
    result = result.replace( "%text%", text );

    return result;
}
void TestResultFormEntry::addImageToHtmlText( QString& text, QString imagePath ) {
    static const QString htmlImageSample = "<img src=\"%img-path%\"/> ";

    QString tmp = htmlImageSample;
    text += tmp.replace( "%img-path%", imagePath );
}
void TestResultFormEntry::show( ) {
    base->show( );
    QWidget::show( );
}
void TestResultFormEntry::hide( ) {
    base->hide( );
    QWidget::hide( );
}
QSize TestResultFormEntry::sizeHint() const {
    return QSize( width( ), height( ) );
}
QSizePolicy	TestResultFormEntry::sizePolicy() const {
    QSizePolicy policy;
    policy.setVerticalPolicy( QSizePolicy::Fixed );
    return policy;
}
