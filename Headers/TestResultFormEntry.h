#ifndef _YV_TESTRESULTFORMENTRY_H
#define _YV_TESTRESULTFORMENTRY_H

#include <QWidget>
#include <QLabel>
#include <QTextEdit>
#include <QString>

class TestResultFormEntry : public QWidget {
    public:
        TestResultFormEntry( bool isRight,
                             QString question,
                             QString selectedVariant,
                             QWidget* parent = nullptr,
                             QString rightAnswer = "" );
        ~TestResultFormEntry( );

        void setGeometry( int x, int y, int width );

        void show( );
        void hide( );

        QSize sizeHint( ) const;
        QSizePolicy	sizePolicy( ) const;
    private:
        enum Color {
            Red,
            Green,
            Blue,
            Black
        };
        enum Weight {
            Normal,
            Bold
        };

        QTextEdit* base;

        QString enumColorToString( Color color );
        QString enumWeightToString( Weight weight );
        void updateTextEditHeight( QTextEdit* te, int width );
        void setupBaseText( bool isRight,
                            QString question,
                            QString selectedVariant,
                            QString rightAnswer );
        QString obtainCustomizedHtmlString( QString text,
                                            int fontSize,
                                            Color fontColor,
                                            Weight fontWeight );
        void addImageToHtmlText( QString& text, QString imagePath );
};

#endif // _YV_TESTRESULTFORMENTRY_H
