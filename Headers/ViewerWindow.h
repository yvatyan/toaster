#ifndef _YV_VIEWERWINDOW_H_
#define _YV_VIEWERWINDOW_H_

#include <QMainWindow>
#include <Headers/TestQuestionForm.h>
#include <Headers/AutoCorrectableLabel.h>
#include <Headers/TestEngine.h>

namespace Ui {
    class ViewerWindow;
}

class ViewerWindow : public QMainWindow {
    Q_OBJECT
    public:
        explicit ViewerWindow( QString testFilePath, QWidget* parent = 0 );
        ~ViewerWindow( );

    public slots:
        void StartTest ( );
        void FirstQuestionHandle( );
        void PreviousQuestion( );
        void NextQuestion( );
        void IncompleteAnswer( bool incomplete );
        void LastQuestionHandle( );
        void FinishTest( );
    private:
        Ui::ViewerWindow* ui;
        TestEngine engine;

        void postInit( );
};

#endif // _YV_VIEWERWINDOW_H_
