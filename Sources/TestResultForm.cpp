#include "Headers/TestResultForm.h"
#include <QPushButton>

TestResultForm::TestResultForm( QWidget* parent ) :
    QWidget( parent )
{
    scrollArea = new QScrollArea( this );
    scrollArea->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    baseWidget = new QWidget;
    vLayout = new QVBoxLayout;
    vLayout->setContentsMargins( 10, 10, 10, 10 );
    vLayout->setSpacing( 5 );
}
TestResultForm::~TestResultForm( ) {
    delete vLayout;
    delete baseWidget;
    delete scrollArea;
}
void TestResultForm::SetHeader( QString headerText ) {

}
void TestResultForm::setGeometry( int x, int y, int width, int height ) {
    QWidget::setGeometry( x, y, width, height );
    scrollArea->setGeometry( 0, 0, width, height );
    QMargins margins = vLayout->contentsMargins( );
    for(int i = 0; i < entries.size( ); ++i ) {
        entries.at( i )->setGeometry( 0, 0, width - margins.left( ) - margins.right( ) - 2 );
    }
    // TO DELETE
    this->setStyleSheet("border: 1px solid orange");
    scrollArea->setStyleSheet("border: 1px solid blue");
    baseWidget->setStyleSheet("border: 1px solid green");
}
void TestResultForm::AddEntry( bool isRight, QString question, QString selectedVariant, QString rightVariant ) {
    TestResultFormEntry* entry = new TestResultFormEntry(
        isRight,
        question,
        selectedVariant,
        static_cast<QWidget*>( parent( ) ),
        rightVariant
    );
    QMargins margins = vLayout->contentsMargins( );
    entry->setGeometry( 0, 0, width( ) - margins.left( ) - margins.right( ) - 20 );
    entry->setStyleSheet( "border: 1px solid purple" );
    vLayout->addWidget( entry, 0, Qt::AlignLeft );
    entries.append( entry );
}
void TestResultForm::show( ) {
    baseWidget->setLayout( vLayout );
    scrollArea->setWidget( baseWidget );
    for( int i = 0; i < entries.size( ); ++i ) {
        entries.at( i )->show( );
    }
    QWidget::show( );
}
void TestResultForm::hide( ) {
    for( int i = 0; i < entries.size( ); ++i ) {
        entries.at( i )->hide( );
    }
    QWidget::hide( );
}
