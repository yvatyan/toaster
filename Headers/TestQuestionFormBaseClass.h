#ifndef _YV_TESTQUESTIONFORMBASECLASS_H_
#define _YV_TESTQUESTIONFORMBASECLASS_H_

#include <QWidget>

class TestQuestionFormBaseClass : public QWidget {
    Q_OBJECT
    public:
        TestQuestionFormBaseClass( QWidget* parent ) : QWidget(parent) {}
        virtual ~TestQuestionFormBaseClass( ) = default;

        virtual void SetQuestion(QString question, const QFont &font ) = 0;
        virtual QString GetQuestion( ) const = 0;
        virtual void AddVariant( QString variant, const QFont &font ) = 0;

        virtual void setGeometry( int x, int y, int width, int height, int boxWidth = -1 ) = 0;
        virtual void setGeometry( QRect boundary, int boxWidth = -1 ) = 0;
        virtual void show( ) = 0;
        virtual void hide( ) = 0;

        virtual QList<QPair<QString, bool> > GetVariants( ) const = 0;
    protected:
        virtual void onAnswerChangeImpl( ) const = 0;
    public slots:
        void onAnswerChange( ) const {
            onAnswerChangeImpl( );
        }
    signals:
        void answerIsEmpty( bool isEmpty ) const;

};

#endif // _YV_TESTQUESTIONFORMBASECLASS_H_
