#include "Headers/CreatorWindow.h"
#include "Headers/ViewerWindow.h"
#include <QApplication>

// #define TEST_VIEWER

int main( int argc, char* argv[ ] ) {
    QApplication a( argc, argv );
    if( argc == 2 ) {
        static ViewerWindow viewer( argv[1] );
        viewer.show( );
    } else {
#ifndef TEST_VIEWER
        static CreatorWindow creator;
        creator.show( );
#else
        QFile::copy( ":/Tests/eng_test_v_1.learn", "~tempFile" );
        QFile::setPermissions( ".\\~tempFile", QFileDevice::WriteOwner );
        static ViewerWindow viewer( ".\\~tempFile" );
        QFile::remove( "~tempFile" );
        viewer.show( );
#endif
    }
    return a.exec( );
}
