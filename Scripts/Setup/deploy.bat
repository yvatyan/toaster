@echo off
::Should be changed MANUALLY
SET projectPath="D:\Projects\Toaster"

mkdir %projectPath%\Setup
del %projectPath%\Setup\* /Q /S /F
for /D %%p in ("Setup\*.*") do rmdir "%%p" /s /q
echo.
echo "Coping files to %projectPath%\Setup directory"
echo.
copy %projectPath%\Build\release\Toaster.exe %projectPath%\Setup
echo ----------------------------------------------------------
echo Deploying application %projectPath%\Setup\Toaster.exe
echo.
windeployqt.exe --force --libdir %projectPath%\Setup --release --compiler-runtime --no-system-d3d-compiler --no-translations %projectPath%\Setup\Toaster.exe
pause