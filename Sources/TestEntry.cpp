#include "Headers/TestEntry.h"
#include <QDateTime>
#include <QDebug>

void TestEntry::SetMainBlock( QString string ) {
    mainBlock = string;
}
void TestEntry::SetQuestionSourceMask( unsigned char srcMask ) {
    questionSourceMask = srcMask;
}
void TestEntry::SetQuestionFormat( Format fmt ) {
    questionFormat = fmt;
}
void TestEntry::SetId( int i ) {
    id = i;
}
QString TestEntry::GetMainBlock( ) const {
    return mainBlock;
}
unsigned char TestEntry::GetQuestionSourceMask( ) const {
    return questionSourceMask;
}
QString TestEntry::GetQuestionSourceMaskStr( ) const {
    QString maskStr;
    for( int i = 0; i < 3; ++i ){
        if( questionSourceMask >> i & 0x01 ) {
            maskStr += "v";
        } else {
            maskStr += "x";
        }
    }
    return maskStr;
}
TestEntry::Format TestEntry::GetQuestionFormat( ) const {
    return questionFormat;
}
char TestEntry::GetQuestionFormatStr( ) const {
    if( questionFormat == SelectOne ) {
        return 'O';
    } else if( questionFormat == SelectMultiple ) {
        return 'M';
    } else if( questionFormat == TextInput ) {
        return 'T';
    }
    return 0;
}
int TestEntry::GetSecondaryBlocksQty( ) const {
    return secondaryBlocks.size();
}
const QList<QString>& TestEntry::GetSecondaryBlocks( ) const {
    return secondaryBlocks;
}
QString TestEntry::GetSecondaryBlockAt( int index ) const {
    return secondaryBlocks.at( index );
}
QString TestEntry::GetRandomSecondaryBlock( int& index ) const {
    qsrand( QDateTime::currentDateTime( ).toMSecsSinceEpoch( ) );
    index = qrand( ) % secondaryBlocks.size( );
    return GetSecondaryBlockAt( index );
}
int TestEntry::GetId( ) const {
    return id;
}
bool TestEntry::IsVariantRight( QString variant ) const {
    bool right = false;
    variant = variant.trimmed( );
    qDebug() << "VARIANT: " << variant;
    for( int i = 0; i < secondaryBlocks.size( ); ++i ) {
        if( secondaryBlocks.at( i ) == variant ) {
            right = true;
            qDebug() << "isRigth";
            break;
        }
    }
    return right;
}
void TestEntry::AddSecondaryBlock( QString string ) {
    secondaryBlocks.append( string );
}
void TestEntry::DeleteSecondaryBlockAt( int index ) {
    secondaryBlocks.removeAt( index );
}
