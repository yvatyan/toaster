#ifndef _YV_AUTOCORRECTABLELABEL_H_
#define _YV_AUTOCORRECTABLELABEL_H_

#include <QLabel>

class AutoCorrectableLabel : public QLabel {
    Q_OBJECT
    public:
        AutoCorrectableLabel( QWidget* parent = 0, Qt::WindowFlags f = 0 )
            : QLabel( parent, f ) {}
        AutoCorrectableLabel( const QString& text, QWidget* parent = 0, Qt::WindowFlags f = 0 )
            : QLabel( text, parent, f) {}

        void AutoCorrectFont( );
        void AutoCorrectBorder( );
};

#endif // _YV_AUTOCORRECTABLELABEL_H_
