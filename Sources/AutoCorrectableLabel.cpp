#include <Headers/AutoCorrectableLabel.h>

void AutoCorrectableLabel::AutoCorrectFont( ) {
    int step = font( ).pointSize( );
    while( step > 0 ) {
        QFontMetrics fontMetrics( font( ) );
        QRect bound = fontMetrics.boundingRect(
                    0,
                    0,
                    width( ),
                    height( ),
                    ( wordWrap() ? Qt::TextWordWrap : 0 ) | alignment( ),
                    text( )
        );
        bool fit = width( ) > bound.width( ) + 2 * margin( ) && height( ) > bound.height( ) + 2 * margin( );
        if( fit ) {
            break;
        }
        step --;
        QFont reducedFont = font();
        reducedFont.setPointSize( step );
        setFont( reducedFont );
    }
}
void AutoCorrectableLabel::AutoCorrectBorder( ) {
    QFontMetrics fontMetrics( font( ) );
    QRect bound = fontMetrics.boundingRect(
                0,
                0,
                width( ),
                height( ),
                ( wordWrap() ? Qt::TextWordWrap : 0 ) | alignment( ),
                text( )
    );
    setGeometry( x( ), y( ), bound.width( ) + margin( ), bound.height( ) + margin ( ) );
}
