#include "Headers/Configuration/Global.h"
#include "Headers/CreatorWindow.h"
#include "ui_CreatorWindow.h"
#include <QFileDialog>

void CreatorWindow::postInit() {
    setWindowIcon( QIcon( ":/Resources/Icons/ToasterIcon.ico" ) );
    ui->toasterLabel->setPixmap( QPixmap( ":/Resources/Logos/Toaster.png" ) );
    ui->toasterLogoLabel->setPixmap( QPixmap( ":/Resources/Logos/ToasterLogo.png" ) );
    ui->versionLabel->setText( Global::version );

    QObject::connect( ui->addToTestButton, SIGNAL( clicked( bool ) ),
                      this, SLOT( ConstructAndAddEntry( ) ) );
    QObject::connect( ui->saveToFileButton, SIGNAL( clicked( bool ) ),
                      this, SLOT( ConstructAndSaveTest( ) ) );
    QObject::connect( ui->browseFileButton, SIGNAL( clicked( bool ) ),
                      this, SLOT( SelectFileFromDialog()) );

    test.SetEntriesVisualizerWidget( ui->testEntriesTree );
}

CreatorWindow::CreatorWindow( QWidget *parent ) :
    QMainWindow( parent ),
    ui( new Ui::CreatorWindow )
{
    ui->setupUi( this );
    postInit( );
}

CreatorWindow::~CreatorWindow( ) {
    delete ui;
}

void CreatorWindow::ConstructAndAddEntry( ) {
    TestEntry entry;

    entry.SetMainBlock( ui->mainBlockLE->text( ) );
    QStringList secondaryBlocks = ui->secondaryBlockPTE->toPlainText().split( '\n' );
    for ( int i = 0; i < secondaryBlocks.size(); ++i ) {
        QString secondaryBlock = secondaryBlocks[ i ].trimmed( );
        if( !secondaryBlock.isEmpty( ) ) {
            entry.AddSecondaryBlock( secondaryBlock );
        }
    }
    TestEntry::Source s1 = ui->mainBlockCB->isChecked() ?
                TestEntry::MainBlock : TestEntry::None;
    TestEntry::Source s2 = ui->firstEntryCB->isChecked() ?
                TestEntry::FirstEntryOfSecBlock : TestEntry::None;
    TestEntry::Source s3 = ui->secondaryBlockCB->isChecked() ?
                TestEntry::SecondaryBlock : TestEntry::None;
    entry.SetQuestionSourceMask( s1 | s2 | s3 );
    if( ui->selectOneRB->isChecked() ) {
        entry.SetQuestionFormat( TestEntry::SelectOne );
    } else if( ui->selectMultipleRB->isChecked() ) {
        entry.SetQuestionFormat( TestEntry::SelectMultiple );
    } else if( ui->textInputRB->isChecked() ) {
        entry.SetQuestionFormat( TestEntry::TextInput );
    }

    test.AddEntry( entry );
}
void CreatorWindow::ConstructAndSaveTest( ) {
    test.SetTestName( ui->testNameLE->text( ) );

    test.SetQuestionsQty( ui->questionsQtyLE->text( ).toInt( ) );

    TestDatabase::Policy answerPolicy = TestDatabase::None;
    if( ui->customPolicyRB->isChecked( ) ) {
        answerPolicy = TestDatabase::Custom;
    } else if( ui->forbidPreviousRB->isChecked( ) ) {
        answerPolicy = TestDatabase::ForbidPrevious;
    }
    test.SetAnswerPolicy( answerPolicy );

    test.EnableForceAnswer( ui->forceAnswerCB->isChecked( ) );

    test.SetFilePath( ui->filePathLE->text( ) );

    test.SaveToFile( );
}
void CreatorWindow::SelectFileFromDialog( ) {
    QString fileName = QFileDialog::getSaveFileName(
                this,
                "Specify test file",
                QDir::homePath() + "\\Desktop",
                "Test Files (*.txt *.test *.learn);;All Files (*.*)"
                // 0, QFileDialog::DontUseNativeDialog); // may work if file dialog hangs
    );
    ui->filePathLE->setText( fileName );
}
