#ifndef _YV_TEST_H_
#define _YV_TEST_H_

#include "Headers/TestEntriesContainer.h"
#include <fstream>

class TestDatabase : public QObject {
    public:
        enum Policy {
            None,
            Custom,
            ForbidPrevious
        };
        TestDatabase( );
        void AddEntry( TestEntry entry );
        void SetEntriesVisualizerWidget( QWidget* dataVisualizerWidget );
        void SetTestName( QString testName );
        void SetQuestionsQty( int quantity );
        void SetAnswerPolicy(Policy policy );
        void EnableForceAnswer(bool force );
        void SetFilePath( QString path );
        void LoadFromFile( );

        TestEntry GetEntryByIndex( int index ) const;
        TestEntry GetRandomEntry( ) const;
        int GetEntriesQty( ) const;
        QString GetTestName( ) const;
        int GetQuestionQty( ) const;
        Policy GetAnswerPolicy( ) const;
        QString GetAnswerPolicyDescription( ) const;
        QList<QPair<QString, QString> > GetTestInfoFields( ) const;
        bool IsAnswerForced( ) const;
        void SaveToFile( ) const;
    private:
        TestEntriesContainer entries;
        QString name;
        int questionQty;
        Policy answerPolicy;
        bool forcedAnswer;
        QString filePath;

        void parseHeaderLine( QString line );
        void parseEntryLine( QString line );
};

#endif // _YV_TEST_H_
